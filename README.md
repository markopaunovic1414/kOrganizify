# kOrganizify

  

![kOrganizify Poster](kOrganizify/resources/images/kOrganizifyPoster.png)

  

## :memo: Opis aplikacije
**kOrganizify** je izuzetno korisna aplikacija koja vam pomaže da efikasno organizujete svoje vreme i zadatke. Sa lakoćom možete postavljati planove, pratiti događaje i održavati visok nivo produktivnosti uz pomoć podsetnika i kalendara.

  

Ako se ikada nađete u situaciji gde niste sigurni kako najbolje da rasporedite svoje obaveze, Planer će to učiniti umesto vas. Unesite događaje i vremenski raspon tokom dana u kom želite da ih obavite i prepustite *SmartPlan-u* da organizije vaše obaveze i predstavi vam sve moguće opcije. 
  

Kada se radi o druženju s prijateljima, Planer vam olakšava dogovaranje susreta. Jednim slanjem zahteva za sinhronizaciju sa svojim prijateljem, aplikacija će automatski pronaći slobodna vremena za vas oboje i predložiti vam odgovarajuće opcije. Kada pronađete idealan termin koji odgovara i vama i vašem prijatelju, Planer će ga automatski upisati u vaš i kalendar vašeg prijatelja. Sve ovo čini organizaciju vremena i društvenih aktivnosti jednostavnom i efikasnom.

  

## :video_camera: Demo snimak projekta

[kOrganizify](https://www.youtube.com/watch?v=osjZg6V7cFo) <br> 

  

## :computer: Okruženje

- [![qtCreator](https://img.shields.io/badge/IDE-Qt_Creator-olivia)](https://www.qt.io/download) <br>

  

## :abc: Programski jezik

- [![c_plus_plus](https://img.shields.io/badge/Language-C%2B%2B-red)](https://www.cplusplus.com/) *C++17*  <br>

- [![qt6](https://img.shields.io/badge/Framework-Qt6-blue)](https://doc.qt.io/qt-6/) *Qt6*  <br>

  

## :books: Korišćene biblioteke

- Qt >= 6.4

- Qt Multimedia

  

## :wrench: Instalacija

- Preuzeti i instalirati [*Qt* i *Qt Creator*](https://www.qt.io/download).

- Ako je to potrebno, nadograditi verziju C++ na C++17 <br>

  
  
## :inbox_tray: Preuzimanje i pokretanje :

-  1. U terminalu se pozicionirati u željeni direktorijum

-  2. Klonirati repozitorijum komandom: `$ git clone https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2023-2024/kOrganizify.git`

-  3. Otvoriti okruzenje *Qt Creator* i u njemu otvoriti CMakeLists.txt fajl

-  4. Pritisnuti dugme *Run* u donjem levom uglu ekrana <br>

-  5. Nakon što se registrujete, možete dodavati i brisati događaje iz kalendara

-  6. Sada možete početi korišćenje planera

-  7. Takođe, imate opciju izbora *SmartPlan* klikom na ikonicu mini kalendara u gornjem desnom uglu.

-  8. Uz projekat dostavljeni su kalendari koje mozete videti u folderu user_data, ime json-a je i samo korisnicko ime i sifra.   
  

### :arrow_forward: Za pokretanje servera :

-  1. Pored CMakeLists.txt fajla, neophodno je otvoriti i CMakeLists.txt fajl iz direktorijuma Server

-  2. Pokrenuti server tako što kliknete na ikonicu *Kompjuter* u donjem levog uglu i izabere Server. Nakon toga kliknete na *Run*.

-  3. Nakon ovoga na isti način izaberite kOrganizify, i pokrenite ga dva puta, jednom kao korisnik, drugi put kao prijatelj.

-  4. Sada možete izabrati prijatelja iz vaše liste prijatelja i započeti proces dogovaranja zajedničkog događaja.

  

## :family: Članovi:

-  <a  href="https://gitlab.com/jelisavetagavrilovic">Jelisaveta Gavrilović 188/2020</a>

-  <a  href="https://gitlab.com/sarakalinic24">Sara Kalinić 387/2021</a>

-  <a  href="https://gitlab.com/andjixi">Anđela Jovanović 205/2020</a>

-  <a  href="https://gitlab.com/markopaunovic1414">Marko Paunović 104/2020</a>

-  <a  href="https://gitlab.com/Markic01">Marko Radosavljević 79/2020</a>
